"use stirct";

/*Creación de las Funciones*/

/*Función para limpiar las cajas de texto*/
const limpiarBtn = () =>{
    document.getElementById("txtValor").value = "";
    document.getElementById("cmbPlanes").value = "-1";
    document.getElementById("txtEnganche").value = "";
    document.getElementById("txtFinanciar").value = "";
    document.getElementById("txtPago").value = "";
};


/*Función para calcular la cotizacion del automovil*/

const calcularBtn = () =>{
    /*Obtener los valores y declarar variables*/
    const opcion = document.getElementById("cmbPlanes").value;
    const valor = parseInt(document.getElementById("txtValor").value);
    let interes, meses;

    /*Validar el valor que se manda*/
    if(valor <= 0 || isNaN(valor)){
        document.getElementById("txtValor").type = "number";
        document.getElementById("txtValor").value = "";
        document.getElementById("cmbPlanes").value = "-1";
        return alert("El valor ingresado no es valido.")
    }

    /*Creación de switch para los planes de credito*/

    switch(opcion){
        case '0': interes = 0.125;
                  meses = 12;
        break;

        case '1': interes = 0.172;
                  meses = 18;
        break;

        case '2': interes = 0.21;
                  meses = 24;
        break;

        case '3': interes = 0.26;
                  meses = 36;
        break;

        case '4': interes = 0.45;
                  meses = 48;
        break;

        default: document.getElementById("cmbPlanes").value = "-1";
                 document.getElementById("txtValor").value = "";
                 return alert("Elija una opción valida");
    }

    /*Calcular los pagos*/
    const enganche = (valor * 0.3).toFixed(2);
    const financiamiento = ((valor - enganche) + ((valor - enganche) * interes)).toFixed(2);
    const pagoMensual = (financiamiento / meses).toFixed(2);

    /*Impresion de los valores*/
    document.getElementById("txtEnganche").value = `$${enganche}`;
    document.getElementById("txtFinanciar").value = `$${financiamiento}`;
    document.getElementById("txtPago").value = `$${pagoMensual}`;


};

/*Llamar a las funciones*/
document.getElementById("btnCalcular").addEventListener("click", calcularBtn);
document.getElementById("btnLimpiar").addEventListener("click", limpiarBtn);